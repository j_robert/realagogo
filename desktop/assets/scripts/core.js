// core.js

var core = (function () {
  'use strict';
  
  function click(e) {
    var id = e.target.id, title = e.target.title, classes = e.target.className;
    
    if (id === 'login-button') {
      pane.open(e.target.getAttribute('data-open'));
    }
    
    if (classes === 'close-button') {
      pane.close(e.target.getAttribute('data-close'));
    }
  }
  
  document.addEventListener('click', click, true);
}());