// pane.js

var pane = (function () {
  'use strict';
  
  return {
    open: function (pane) {
      if (pane) {
        document.getElementById(pane).className += ' show';
        this.backdrop('show', pane);
      }
    },
    
    close: function (pane) {
      if (pane) {
        document.getElementById(pane).className = 'pane';
        this.backdrop('hide');
      }
    },
        
    backdrop: function (state, pane) {
      var backdrop = document.createElement('div');
      backdrop.id = 'backdrop';
      
      if (state === 'show') {
        pane = document.getElementById(pane);
        pane.parentNode.insertBefore(backdrop, pane.nextSibling);
      } else {
        backdrop = document.getElementById('backdrop');
        backdrop.className = 'hide';
        setTimeout(function () { backdrop.remove(); }, 300);
      }
    }
  };
}());