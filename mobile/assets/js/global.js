 // GO //

(function () {
  'use strict';
  
  var standalone = 'standalone';
  
  // Detect Apple devices
  function isApple() {
    return (
      (navigator.userAgent.match(/iPad/i) !== null) ||
      (navigator.platform.indexOf('iPhone') !== -1) ||
      (navigator.platform.indexOf('iPod') !== -1)
    );
  }
  
  // Detect iOS WebView mode
  (function () {
    if (isApple()) {
      if ((standalone in window.navigator) &&
          !window.navigator.standalone) {
        return 0;
      } else {
        document.location = 'details.html';
      }
    }
  }());
}());